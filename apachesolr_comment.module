<?php

/**
 * @file
 * Indexer for the comment entities for the Apachesolr module.
 */

/**
 * Implements hook_apachesolr_entity_info_alter().
 */
function apachesolr_comment_apachesolr_entity_info_alter(&$entity_info) {
  $entity_info['comment']['indexable'] = TRUE;
  $entity_info['comment']['status callback'][] = 'apachesolr_comment_status_callback';
  $entity_info['comment']['document callback'][] = 'apachesolr_comment_index_solr_document';
  $entity_info['comment']['reindex callback'] = 'apachesolr_comment_solr_reindex';
  $entity_info['comment']['index_table'] = 'apachesolr_index_entities_comment';

  // @TODO we need to work on the cron synchronization call back.
}

/**
 * Comment specific status callback.
 */
function apachesolr_comment_status_callback($entity_id, $entity_type) {
  $comment = comment_load($entity_id);
  return ($comment->status == 1);
}

/**
 * Builds the comment-specific information for a Solr document.
 *
 * @param ApacheSolrDocument $document
 *   The Solr document we are building up.
 * @param object $comment
 *   The entity we are indexing.
 * @param string $entity_type
 *   The type of entity we're dealing with.
 */
function apachesolr_comment_index_solr_document(ApacheSolrDocument $document, $comment, $entity_type) {  
  $document->cid = $comment->cid;
  if (!empty($comment->pid)) {
    $document->is_comment_pid = $comment->pid;
  }
  $document->label = $comment->subject;
  $document->is_comment_nid = $comment->nid;
  $document->is_comment_uid = $comment->uid;
  $document->ss_name = $comment->name;
  $document->is_comment_timestamp = $comment->created;
  $document->is_comment_changed_timestamp = $comment->changed;
  $document->ss_node_type = $comment->node_type;
  $document->ss_comment_body = $comment->comment_body[LANGUAGE_NONE][0]['value'];

  $documents = array();
  $documents[] = $document;
  return $documents;
}

/**
 * Comment specific reindex call-back.
 */
function apachesolr_comment_solr_reindex() {
  $indexer_table = apachesolr_get_indexer_table('comment');
  $transaction = db_transaction();
  $env_id = apachesolr_default_environment();
  try {
    db_delete($indexer_table)
      ->condition('entity_type', 'comment')
      ->execute();

    // We know there's only one bundle type, so if that doesn't get indexed just
    // skip this entirely.
    if (apachesolr_get_index_bundles($env_id, 'comment')) {
      $select = db_select('comment', 'c');
      $select->join('node', 'n', 'n.nid = c.nid');
      $select->addExpression("'comment'", 'entity_type');
      $select->addExpression("CONCAT('comment_node_', type)", 'bundle');
      $select->addField('c', 'cid', 'entity_id');
      $select->addField('c', 'status', 'status');
      $select->addExpression(REQUEST_TIME, 'changed');

      db_insert($indexer_table)
        ->fields(array(
          'entity_id',
          'status',
          'entity_type',
          'bundle',
          'changed',
          )
        )
        ->from($select)
        ->execute();
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    watchdog_exception('Apache Solr', $e);
    return FALSE;
  }

  return TRUE;
}
